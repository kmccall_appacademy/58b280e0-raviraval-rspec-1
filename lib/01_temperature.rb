def ctof(celsius)
  (celsius.to_f * 9/5) + 32
end


def ftoc(fahrenheit)
  (fahrenheit.to_f - 32) * 5/9
end
