def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(array_of_params)
  return 0 if array_of_params.empty?
  array_of_params.reduce(:+)
end
