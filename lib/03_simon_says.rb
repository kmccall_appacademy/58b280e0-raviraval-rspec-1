def echo(str_to_echo)
  str_to_echo
end

def shout(str_to_shout)
  str_to_shout.upcase
end

def start_of_word(word)
  word.split[0]
end

def first_word(list_of_words)
  list_of_words.split(" ")[0]
end

def titleize(words)
  little_words = ["and", "of", "over", "by", "the"]
  res = []
  words.split(" ").each_with_index do |word, idx|
    if idx == 0 || idx == words.length - 1
      res << word.capitalize
    elsif little_words.include?(word)
      res << word
    else
      res << word.capitalize
    end
  end
  res.join(" ")
end

def start_of_word(word, letters_to_return)
  word[0...letters_to_return]
end

def repeat(thing_to_repeat, num_repeats = 2)
  res = thing_to_repeat
  idx = 0
  while idx < num_repeats - 1
    res += " #{thing_to_repeat}"

    idx += 1
  end
  res
end
