#require 'byebug'
def translate(str)
  str.split(" ").map { |word| pig_word(word)}.join(" ")
end

def pig_word(word)
  if starts_with_vowel?(word)
    word += "ay"
  else
    count = beginning_consonants_number(word)
    word = word[count..-1] + word[0...count] + "ay"
  end
  word
end

def starts_with_vowel?(str)
  vowels = "aeiouAEIOU"
  if vowels.include?(str[0])
    return true
  end
  false
end

def beginning_consonants_number(str)
  #debugger
  consonants = "bcdfghjklmnpqrstvwxyz"
  consonants += consonants.upcase
  count = 0
  until !consonants.include?(str[count + 1])
    count += 1
  end
  count += 1
  idx = 0
  len = count
  qs = "qQ"
  while idx <= len
    if qs.include?(str[idx]) && str[idx + 1] == "u"
      count += 1
    end

    idx += 1
  end
  count
end
